# Fit routines
The purpose of this repository is to collect some easy to use fitting routines for python. Especially, for situations where multiple weights on the data and the axes are required.

* using Orthogonal Distance Regression (ODR):
  ** *gaussian.py* - fitting a single univariate Gaussian with binning- and value uncertainties 