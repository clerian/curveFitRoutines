#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function, division

import numpy as np
from scipy import stats
from scipy.odr import ODR, Model, RealData

def gauss(c,X):
    #c[0]..amplitude
    #c[1]..location
    #c[2]..sigma    
    return c[0]*stats.norm.pdf(x=X, loc=c[1], scale=c[2])

def plotGaussResult(output,X):
    c = output.beta
    return gauss(c,X)

def fit(data, r):
    # data is unbinned
    # r is a list for the range
    
    binSize = 2*(data.quantile(0.75)-data.quantile(0.25))/(len(data)**(1/3.)) # Freedmann-Diaconis
    nBins   = (r[1]-r[0])/binSize  

    hist, edges = np.histogram(a=data, bins=nBins, range=r)
    binCentre   = edges[:-1]+(edges[1]-edges[0])/2
    model       = Model(gauss)

    xErrors     = np.ones_like(binCentre)*binSize/2.
    yErrors     = np.sqrt(hist) # Poisson error

    yErrors[yErrors==0.] = 999999999999 # ignore everything with zero counts
    
    fitData     = RealData(x=binCentre, y=hist, sx=xErrors, sy=yErrors)

    myodr = ODR(fitData, model, beta0=np.array([np.max(hist), binCentre[np.argmax(hist)], np.std(saneToF)]))
    #myodr.set_job(fit_type=2)
    myoutput = myodr.run()
    myoutput.pprint() # optional

    return myoutout
    #plt.errorbar(binCentre[:-1], hist,xerr=xErrors, yerr=np.sqrt(hist),fmt='o')
    #x=np.linspace(r[0], r[1], 20000)
    #plt.plot( x,plotGaussResult(myoutput,x))

